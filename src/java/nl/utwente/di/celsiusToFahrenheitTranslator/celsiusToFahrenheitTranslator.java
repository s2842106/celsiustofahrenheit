package nl.utwente.di.celsiusToFahrenheitTranslator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class celsiusToFahrenheitTranslator extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private nl.utwente.di.celsiusToFahrenheitTranslator.translator translator;
	
    public void init() throws ServletException {
    	translator = new translator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Celsius to Fahrenheit translator";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius: " +
                   request.getParameter("Celsius") + "\n" +
                "  <P>Fahrenheit: " +
                   Double.toString(translator.getfahrenheit(request.getParameter("Celsius"))) +
                "</BODY></HTML>");
  }
  

}
