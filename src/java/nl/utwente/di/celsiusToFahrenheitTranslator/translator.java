package nl.utwente.di.celsiusToFahrenheitTranslator;

public class translator {
    public double getfahrenheit(String Celsius) {
        Double Fahrenheit = Double.valueOf(Celsius)*1.8+32;
        return Fahrenheit;
    }
}